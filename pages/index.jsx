import Head from "next/head";
import Image from "next/image";
import Toolbar from "../src/components/toolbar";
import styles from "../styles/Home.module.css";

export default function Home() {
	return (
		<div>
			<Toolbar />
			<div> Hello world</div>
		</div>
	);
}
