import { useState, useEffect } from "react";
import Toolbar from "../../src/components/toolbar";
import imageUrlBuilder from "@sanity/image-url";
import Link from "next/link";
import {useRouter} from 'next/router'

const Posts = ({ posts }) => {

	const router = useRouter();

	const [mappedPosts, setMappedPosts] = useState([]);

	useEffect(() => {
		if (posts.length) {
			const imgBuilder = imageUrlBuilder({
				projectId: "vb50tyci",
				dataset: "production",
			});

			setMappedPosts(
				posts.map((p) => {
					return {
						...p,
						mainImage: imgBuilder
							.image(p.mainImage)
							.width(500)
							.height(250),
					};
				})
			);
		} else {
			setMappedPosts([]);
		}
	}, [posts]);
	return (
		<div>
			<Toolbar />
			<div>
				<h1>Welcome to my blog</h1>
				<h3>Recent Posts:</h3>
				<div>
					{mappedPosts.length ? (
						mappedPosts.map((p, i) => (
							<div key={i} onClick={()=>router.push(`/posts/${p.slug.current}`)}>
								<h3>{p.title}</h3>
								<img src={p.mainImage}></img>
							</div>
						))
					) : (
						<>No posts yest</>
					)}
				</div>
			</div>
		</div>
	);
};

export default Posts;

export const getServerSideProps = async (pageContext) => {
	const query = encodeURIComponent(`*[ _type == "post" ]`);
	const url = `https://vb50tyci.api.sanity.io/v1/data/query/production?query=${query}`;
	const result = await fetch(url).then((res) => res.json());

	if (!result || !result.result.length) {
		return {
			props: {
				posts: [],
			},
		};
	} else {
		return {
			props: {
				posts: result.result,
			},
		};
	}
};
