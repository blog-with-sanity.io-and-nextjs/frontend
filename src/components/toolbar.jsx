import Link from "next/link";

const Toolbar = () => {
	return (
		<div>
			<Link href="/" scroll={false} passHref={true}>
				<a id="logoFlat">
					Home
				</a>
			</Link>
			<Link href="/posts/" scroll={false} passHref={true}>
				<a id="logoFlat">
					Posts
				</a>
			</Link>
		</div>
	);
};

export default Toolbar;
